#include "liidni.hpp"

ofMesh edge_setup(const ofImage &src) {
    // edge set_up
    ofImage im = src;
    im.setImageType(OF_IMAGE_GRAYSCALE); // Convert to grayscale
    cv::Mat inputMat = ofxCv::toCv(im);
    cv::Mat edgeMat;
    cv::Canny(inputMat, edgeMat, 50, 180); // Set appropriate thresholds

    ofMesh mesh;
    mesh.setMode(OF_PRIMITIVE_POINTS);
    for (int y = 0; y < edgeMat.rows; y++) {
        for (int x = 0; x < edgeMat.cols; x++) {
            if (edgeMat.at<uchar>(y, x) > 0) { // Edge pixel found
                mesh.addVertex(ofPoint(x, y));
            }
        }
    }
    return mesh;
}

ofMesh mesh_color_setup(const ofImage& src, const float intensityThreshold) {
    ofMesh l_mesh;
    l_mesh.setMode(OF_PRIMITIVE_POINTS);

    ofImage im = src;

    int max_width = 512;
    float aspectRatio = im.getWidth() / im.getHeight();
    int newHeight = max_width / aspectRatio;
    im.resize(512, newHeight);

    int w = im.getWidth();
    int h = im.getHeight();
        
    float intensity = 255;

    for (int x=0; x < w; ++x) {
        for (int y=0; y < h; ++y) {
            ofColor c = im.getColor(x, y);
            intensity = c.getLightness();

            if (intensity >= intensityThreshold) {
                // colored_dot new_dot;
                // new_dot.color = c;
                
                float saturation = c.getSaturation();
                // float z = ofMap(saturation, 0, 255, -100, 100);
                l_mesh.addVertex(ofPoint(x, y));
                l_mesh.addColor(c);
            }
        }
    }
    return l_mesh;
}

Liidni::Liidni() {
    ofImage im;
    im.load("default_texture.png");
    init(im);
}

void Liidni::init(const ofImage& im) {
    mesh.clear();
    base_pos.clear();
    offsets_1.clear();
    ofVec2f m_resolution = ofVec2f(im.getWidth(), im.getHeight());
    
    mesh.setMode(OF_PRIMITIVE_POINTS);
    mesh = mesh_color_setup(im);
    // reduce_vertices(10000);
    record_offsets_and_base_position();

    std::cout << "/nsdafg/n" << m_resolution.x << ":" << m_resolution.y << std::endl;
    m_fbo.allocate(std::max(m_resolution.x, float(10.)), std::max(float(10.), m_resolution.y), GL_RGBA);
    m_fbo.begin();
    ofClear(0,0,0);
    mesh.draw();
    m_fbo.end();
}

void Liidni::update() {
    m_fbo.begin();
    ofClear(0,0,0);
    mesh.draw();
    m_fbo.end();
}

void Liidni::draw() {
    mesh.draw();
}

ofFbo Liidni::get_fbo() {
    return m_fbo;
}

void Liidni::reduce_vertices(const int max_number) {
    // remove vertices if too many
    int randomIndex;
    while (mesh.getNumVertices() > max_number) {
        randomIndex = ofRandom(0, mesh.getNumVertices());
        mesh.removeVertex(randomIndex);
    }
}

void Liidni::record_offsets_and_base_position() {    // adding offset noise
    int num_vertices = mesh.getNumVertices();
    for (int i = 0; i < num_vertices; i++) {
        // mesh.addColor(ofColor::black); // Set each vertex color to black ????
        offsets_1.push_back(ofVec3f(ofRandom(0,100000), ofRandom(0,100000), ofRandom(0,100000)));
        base_pos.push_back(mesh.getVertex(i));
     }
}
