#ifndef liidnimate_hpp
#define liidnimate_hpp

#include "ofMain.h"
#include "liidni.hpp"

namespace liidnimate {

    void jitter(Liidni &liidni, const ofVec2f& position);
    void turbulent(Liidni &liidni, const float strength, const float scale);
    void focus_object(Liidni &liidni, const ofVec2f& object_position, const float attractionStrength);
}

#endif