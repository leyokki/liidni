#include "liidnimate.hpp"

namespace liidnimate {
    
    void jitter(Liidni& liidni, const ofVec2f& position) {
        ofVec3f timeOffsets;
        ofVec3f l_vertex;
        ofVec3f l_position;

        float time = ofGetElapsedTimef();
        float x_factor = 100 * position.x / ofGetWindowWidth();
        float y_factor = 100 * position.y / ofGetWindowHeight();
        int num_vert = liidni.mesh.getNumVertices();
        
        for (int i = 0; i < num_vert; i++) {
            timeOffsets = liidni.offsets_1[i];
            l_vertex = liidni.mesh.getVertex(i);
            l_position = liidni.base_pos[i];
            l_vertex.x = l_position.x + (x_factor + (timeOffsets.y/50000)) * cos(10 * time + timeOffsets.x);
            l_vertex.y = l_position.y + (y_factor + (timeOffsets.y/50000)) * sin(10 * time + timeOffsets.y);

            liidni.mesh.setVertex(i, l_vertex);
        }
    }

    void turbulent(Liidni& liidni, const float strength, const float scale) {
        float time = ofGetElapsedTimef();

        float noiseStrength = 10 * strength / ofGetWindowWidth();
        float noiseScale = 10 * scale / ofGetWindowHeight();
        int num_vert = liidni.mesh.getNumVertices();

        for (int i = 0; i < num_vert; i++) {
            ofVec3f vertex = liidni.mesh.getVertex(i);

            // Calculate noise value
            float noiseValue = ofNoise(vertex.x * noiseScale, vertex.y * noiseScale, vertex.z * noiseScale, time);

            // Apply the noise to the vertex position
            vertex.x += (noiseValue - 0.5) * noiseStrength;
            vertex.y += (noiseValue - 0.5) * noiseStrength;
            vertex.z += (noiseValue - 0.5) * noiseStrength;

            liidni.mesh.setVertex(i, vertex);
        }
    }


    void focus_object(Liidni& liidni, const ofVec2f& object_position, const float attractionStrength) {
        float mouseX = object_position.x;
        float mouseY = object_position.y;

        float thresh = 2;
        int num_vert = liidni.mesh.getNumVertices();

        for (int i = 0; i < num_vert; i++) {
            ofVec3f vertex = liidni.mesh.getVertex(i);
            float distance = ofDist(mouseX, mouseY, vertex.x, vertex.y);

            if (distance > thresh) { // Define a threshold for the effect
                ofVec2f direction = ofVec2f(vertex.x, vertex.y) - ofVec2f(mouseX, mouseY);
                direction += liidni.offsets_1[i];
                direction.normalize();
                direction *= -4.;
                liidni.mesh.setVertex(i, vertex - direction * attractionStrength / distance);
            }
        }
    }
}