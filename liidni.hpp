#ifndef liidni_hpp
#define liidni_hpp

#include "ofMain.h"
#include "ofxCv.h"
#include <algorithm>

ofMesh mesh_color_setup(const ofImage& im, const float intensityThreshold = 50.0);
ofMesh mesh_edges_setup(const ofImage& im);

struct Liidni {
    ofMesh mesh;
    ofFbo m_fbo;
    std::vector<ofVec3f> offsets_1;
    std::vector<ofVec3f> base_pos;

    std::vector<std::vector<int>> full_board;
    ofVec2f m_resolution = ofVec2f(1,1);
    // check if full_board is updated
    // rebuild sand::

    bool is_built = false;

    Liidni();
    
    void init(const ofImage& im);
    void update();
    ofFbo get_fbo();

    void draw();
    void record_offsets_and_base_position();
    void reduce_vertices(const int max_number = 5000);
};

#endif